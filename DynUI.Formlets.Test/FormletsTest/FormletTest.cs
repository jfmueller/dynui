﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DynUI.Formlets;
using NUnit.Framework;

namespace DynUI.Formlets.Test.FormletsTest
{
    [TestFixture]
    public sealed class FormletTest
    {
        [Test]
        public void CreateFormlet()
        {
            var fl = Formlet.Create(counter =>
                new FormletResult<string>()
                {
                    XamlElements = new List<XElement>() {
                        new XElement("input", new XAttribute("name", "frmel" + counter))
                    },
                    Collector = env => env["frmel" + counter],
                    Counter = counter + 1
                });
            var result = fl.Run(10);
            Assert.AreEqual(1, result.XamlElements.Count());
            var environment = new DynUI.Formlets.Environment();
        }
    }
}
