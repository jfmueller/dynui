﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DynUI.Formlets;
using System.ComponentModel;

namespace DynUI.Formlets.Test.FormletsTest
{
    [TestFixture]
    public sealed class ValueChangedTest
    {
        [Test]
        public void SetHasValueToFalseTest()
        {
            var it = new ValueChanged<string>(new Dictionary<string, INotifyPropertyChanged>());
            Assert.IsFalse(it.HasValue);
        }

        [Test]
        public void SetValueToTrueTest()
        {
            var it = new ValueChanged<string>(new Dictionary<string, INotifyPropertyChanged>(), "value");
            Assert.IsTrue(it.HasValue);
            Assert.AreEqual("value", it.Value);
        }
    }
}
