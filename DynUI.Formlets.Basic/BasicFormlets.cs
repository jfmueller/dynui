﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynUI.Formlets
{
    public static class BasicFormlets
    {
        private static XNamespace xamlDefaultNamespace = "http://schemas.microsoft.com/winfx/2006/xaml/presentation";

        /// <summary>
        /// Display some text.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Formlet<Unit> Text(string s)
        {
            return Formlet.Create(i => new FormletResult<Unit>
            {
                XamlElements = new List<XNode> { new XElement(xamlDefaultNamespace + "TextBlock", s) },
                HtmlElements = new List<XNode> { new XText(s) },
                Collector = env => Unit.Value,
                Counter = i + 1
            });
        }

        /// <summary>
        /// Create a grid for arranging elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Formlet<T> Grid<T>(Formlet<T> input)
        {
            return Formlet.Create(i =>
                {
                    var v1 = input.Run(i);
                    return new FormletResult<T>
                    {
                        XamlElements = new List<XNode> { new XElement(xamlDefaultNamespace + "Grid", v1.XamlElements) },
                        HtmlElements = new List<XNode> { new XElement("div", v1.HtmlElements) },
                        Collector = env => v1.Collector(env),
                        Counter = v1.Counter
                    };
            });
        }

        /// <summary>
        /// Create a vertical stack of elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Formlet<T> VerticalStack<T>(Formlet<T> input)
        {
            return Formlet.Create(i =>
            {
                var v1 = input.Run(i);
                var stackPanel = new XElement(xamlDefaultNamespace + "StackPanel", v1.XamlElements);
                stackPanel.SetAttributeValue(xamlDefaultNamespace + "Orientation", "Vertical");
                return new FormletResult<T>
                {
                    XamlElements = new List<XNode> { stackPanel },
                    HtmlElements = v1.HtmlElements.Select(elem => new XElement("div", elem)),
                    Collector = env => v1.Collector(env),
                    Counter = v1.Counter
                };
            });
        }

    }
}
