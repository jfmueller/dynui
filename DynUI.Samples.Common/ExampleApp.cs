﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynUI.Formlets;

namespace DynUI.Samples.Common
{
    public static class ExampleApp
    {
        public static Formlet<Unit> HomePage()
        {
            var page = from pg in BasicFormlets.Text("This is some text")
                       join t in BasicFormlets.Text("This is a second line of text") on 1 equals 1
                       select Unit.Value;
            return BasicFormlets.VerticalStack(page);
        }
    }
}
