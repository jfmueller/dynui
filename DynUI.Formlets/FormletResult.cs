﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynUI.Formlets
{
    public class FormletResult<T>
    {
        public IEnumerable<XNode> XamlElements { get; set; }

        public IEnumerable<XNode> HtmlElements { get; set; }

        public Func<Environment, T> Collector { get; set; }

        public int Counter { get; set; }
    }
}
