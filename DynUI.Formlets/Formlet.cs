﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynUI.Formlets
{
    public class Formlet<T>
    {
        public Func<int, FormletResult<T>> Run { get; set; }

        public XDocument RenderXamlWindow(string title, int width = 350, int height = 350)
        {
            XNamespace ns = "http://schemas.microsoft.com/winfx/2006/xaml/presentation";
            var root = new XElement(ns + "Window", Run(0).XamlElements);
            root.SetAttributeValue("xmlns", ns.NamespaceName);
            root.SetAttributeValue("Height", height);
            root.SetAttributeValue("Width", width);
            var doc = new XDocument(root);
            return doc;
        }

        public XDocument RenderHTML()
        {
            return new XDocument(new XElement("div", Run(0).HtmlElements));
        }

        public T Evaluate(Environment env)
        {
            return Run(0).Collector(env);
        }
    }

    public static class Formlet
    {
        public static Formlet<T> Create<T>(Func<int, FormletResult<T>> f)
        {
            return new Formlet<T> { Run = f };
        }

        public static Formlet<Tuple<T1, T2>> Merge<T1, T2>(this Formlet<T1> first, Formlet<T2> second)
        {
            return Formlet.Create(i =>
            {
                var v1 = first.Run(i);
                var v2 = second.Run(v1.Counter);
                return new FormletResult<Tuple<T1, T2>>
                {
                    XamlElements = Enumerable.Concat(v1.XamlElements, v2.XamlElements),
                    HtmlElements = Enumerable.Concat(v1.HtmlElements, v2.HtmlElements),
                    Collector = env => Tuple.Create(v1.Collector(env), v2.Collector(env)),
                    Counter = v2.Counter
                };
            });
        }

        public static Formlet<T> Return<T>(T v)
        {
            return Formlet.Create(i => new FormletResult<T>
            {
                XamlElements = Enumerable.Empty<XElement>(),
                HtmlElements = Enumerable.Empty<XElement>(),
                Collector = env => v,
                Counter = i
            });
        }

        public static Formlet<R> Select<T, R>(this Formlet<T> source, Func<T, R> func)
        {
            return Formlet.Create(i =>
            {
                var value = source.Run(i);
                return new FormletResult<R>
                {
                    XamlElements = value.XamlElements,
                    HtmlElements = value.HtmlElements,
                    Collector = env => func(value.Collector(env)),
                    Counter = value.Counter
                };
            });
        }

        public static Formlet<TResult> Join<TOuter, TInner, TKey, TResult>
          (this Formlet<TOuter> outer, Formlet<TInner> inner,
            Func<TOuter, TKey> outerKeySelector,
            Func<TInner, TKey> innerKeySelector,
            Func<TOuter, TInner, TResult> resultSelector)
        {
            return outer.Merge(inner).Select(tup => resultSelector(tup.Item1, tup.Item2));
        }

    }
}
