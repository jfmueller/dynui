﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynUI.Formlets
{
    /// <summary>
    /// Key-value-store 
    /// </summary>
    public sealed class Environment : Dictionary<string, string>
    {
        public Environment(IEnumerable<Tuple<string, string>> values)
            :base()
        {
            foreach (var tpl in values)
            {
                this.Add(tpl.Item1, tpl.Item2);
            }
        }

        public Environment()
            :base()
        {
        }
    }
}
