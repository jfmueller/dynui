﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynUI.Formlets
{
    public sealed class ValueChanged<T>
    {
        private readonly bool hasValue;

        private readonly IDictionary<string, INotifyPropertyChanged> fields;

        private readonly T value;

        public ValueChanged(IDictionary<string, INotifyPropertyChanged> fields, T value)
        {
            hasValue = true;
            this.fields = fields;
            this.value = value;
        }

        public ValueChanged(IDictionary<string, INotifyPropertyChanged> fields)
        {
            hasValue = false;
            this.fields = fields;
        }

        public bool HasValue { get { return hasValue; } }

        public T Value { get { return value; } }

        public IDictionary<string, INotifyPropertyChanged> Fields { get { return fields; } }
    }

    public static class ValueChanged
    {
        public static ValueChanged<A> Where<A>(this ValueChanged<A> valueChanged, Predicate<A> filter)
        {
            return valueChanged.HasValue
                ? filter(valueChanged.Value)
                    ? valueChanged
                    : new ValueChanged<A>(valueChanged.Fields)
                : valueChanged;
        }
    }
}
