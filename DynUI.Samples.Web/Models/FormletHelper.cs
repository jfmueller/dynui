﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace DynUI.Samples.Web.Models
{
    public static class FormletHelper
    {
        public static IHtmlString ToHtmlString(XDocument doc)
        {
            return new HtmlString(doc.ToString());
        }
    }
}